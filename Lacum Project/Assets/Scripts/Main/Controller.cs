﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{

    public bool agaixado;
    public static bool podeAgaixar = true, podeSeMover = true;
    private float MoveSpeed = 4;

    [SerializeField] private float WalkSpeed, RunSpeed;
    [SerializeField] private float runBuildUp;

    private CharacterController charController;

    public float JumpSpeed = 8.0f;
    public float Gravity = 20.0f;
    private Vector3 moveDirection = Vector3.zero;


    private void Awake()
    {
        charController = this.GetComponent<CharacterController>();
    }

    private void Update()
    {

        PlayerMovement();
        Agaixar();

        if (agaixado == true)
        {
            charController.height = 1;
        }

        if (agaixado == false)
        {
            charController.height = 2;
        }

        #region UpdateKnockback

        if (impact.magnitude > 0.2) charController.Move(impact * Time.deltaTime);
        {
            impact = Vector3.Lerp(impact, Vector3.zero, 5 * Time.deltaTime);
        }

        #endregion


        #region Pulo

        if (charController.isGrounded)
        {
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = JumpSpeed;
            }
        }

        // Apply gravity
        moveDirection.y = moveDirection.y - (Gravity * Time.deltaTime);

        // Move the controller
        charController.Move(moveDirection * Time.deltaTime);
        #endregion
    }

    private void PlayerMovement()
    {
        if (podeSeMover == true)
        {
            float hortInput = Input.GetAxisRaw("Horizontal") * MoveSpeed;
            float vertInput = Input.GetAxisRaw("Vertical") * MoveSpeed;

            Vector3 forwardMovement = transform.forward * vertInput;
            Vector3 rightMovement = transform.right * hortInput;

            charController.SimpleMove(Vector3.ClampMagnitude(forwardMovement + rightMovement, 1.0f * MoveSpeed));
            SetMovement();
        }

    }

    private void SetMovement()
    {
        if (Input.GetButton("RunKey"))
        {
            MoveSpeed = Mathf.Lerp(MoveSpeed, RunSpeed, Time.deltaTime * runBuildUp);
        }
        else
            MoveSpeed = Mathf.Lerp(MoveSpeed, WalkSpeed, Time.deltaTime * runBuildUp);
    }

    private void Agaixar()
    {
        if (podeAgaixar == true && podeSeMover == true)
        {
            if (Input.GetKeyDown("c"))
            {
                agaixado = !agaixado;

            }
        }



    }

    #region Knockback

    public float massa = 3f;
    Vector3 impact = Vector3.zero;

    public void Impacto(Vector3 direcao, float forca)
    {
        direcao.Normalize();
        if (direcao.y < 0) direcao.y = -direcao.y;
        impact += direcao * forca / massa;
    }

    #endregion
}