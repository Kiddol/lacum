﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {

    [SerializeField] private float mouseSensivity;

    [SerializeField] private Transform PlayerBody;
    public static bool Podeolhar = true;
    private float xAxisClamp;

    private void Awake()
    {
        LockCursor();
        xAxisClamp = 0.0f;
    }

    private void LockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        CameraRotation();
        

    }

    private void CameraRotation()
    {

        if (Podeolhar == true)
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensivity  * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensivity  * Time.deltaTime;

            xAxisClamp += mouseY;

            if (xAxisClamp > 90.0f)
            {
                xAxisClamp = 90.0f;
                mouseY = 0.0f;
                ClampXAxisRotationToValue(270.0f);
            }
            else if (xAxisClamp < -90.0f)
            {
                xAxisClamp = -90.0f;
                mouseY = 0.0f;
                ClampXAxisRotationToValue(90.0f);
            }
            transform.Rotate(Vector3.left * mouseY);

            PlayerBody.Rotate(Vector3.up * mouseX);
        }
        


    }

    private void ClampXAxisRotationToValue(float value)
    {
        Vector3 eulerRotation = transform.eulerAngles;
        eulerRotation.x = value;
        transform.eulerAngles = eulerRotation;
    }
}
