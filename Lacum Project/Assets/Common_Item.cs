﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Common_Item : MonoBehaviour
{
    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.LookAt(Player.transform);
    }

    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Vector3 dir = collision.transform.position - this.transform.position;
            dir = dir.normalized;

            collision.gameObject.GetComponent<Controller>().Impacto(dir, 100);
            Debug.Log("Reconhece");
        }
    }

}
